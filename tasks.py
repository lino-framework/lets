from atelier.invlib import setup_from_tasks

ns = setup_from_tasks(
    globals(),
    "lino_lets",
    languages="en de fr".split(),
    # tolerate_sphinx_warnings=True,
    locale_dir='lino_lets/lib/lets/locale',
    revision_control_system='git',
    cleanable_files=['docs/api/lino_lets.*'],
    demo_projects=['lino_lets.projects.letsdemo'])
