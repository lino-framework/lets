=========================
The ``lino-lets`` package
=========================





A Lino application for people who exchange goods and services without money.

This repository is used by the
`Lino LETS tutorial
<https://www.lino-framework.org/dev/lets/index.html>`__.



