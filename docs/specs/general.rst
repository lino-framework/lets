.. doctest docs/specs/general.rst
.. _lets.specs.general:

==============================
General overview of Lino LETS
==============================

The goal of Lino LETS is

.. contents::
  :local:


.. include:: /../docs/shared/include/tested.rst

>>> import lino
>>> lino.startup('lino_lets.projects.letsdemo.settings.doctests')
>>> from lino.api.doctest import *


Show the list of members:

>>> rt.show(users.AllUsers)
... #doctest: +NORMALIZE_WHITESPACE -REPORT_UDIFF
============ ===================== ========== ================================== =================================
 First name   e-mail address        Place      offered_products                   wanted_products
------------ --------------------- ---------- ---------------------------------- ---------------------------------
 Anne         anne@example.com      Tallinn    `Buckwheat <…>`__
 Argo         argo@example.com      Haapsalu   `Electricity repair work <…>`__
 Fred         fred@example.com      Tallinn    `Bread <…>`__, `Buckwheat <…>`__
 Henri        henri@example.com     Tallinn    `Electricity repair work <…>`__    `Buckwheat <…>`__, `Eggs <…>`__
 Jaanika      jaanika@example.com   Tallinn
 Katrin       katrin@example.com    Vigala
 Mari         mari@example.com      Tartu                                         `Eggs <…>`__
 Peter        peter@example.com     Vigala
 Robin        demo@example.com
============ ===================== ========== ================================== =================================
<BLANKLINE>


The `Products` table shows all products in alphabetical order:

>>> rt.show(market.Products)
... #doctest: +NORMALIZE_WHITESPACE -REPORT_UDIFF
==== ========================= ============================= =============================
 ID   Designation               Offered by                    Wanted by
---- ------------------------- ----------------------------- -----------------------------
 1    Bread                     `Fred <…>`__
 2    Buckwheat                 `Anne <…>`__, `Fred <…>`__    `Henri <…>`__
 5    Building repair work
 3    Eggs                                                    `Henri <…>`__, `Mari <…>`__
 6    Electricity repair work   `Argo <…>`__, `Henri <…>`__
 4    Sanitary repair work
==== ========================= ============================= =============================
<BLANKLINE>


The `Offers` table show all offers.

>>> rt.show(market.Offers)
... #doctest: +NORMALIZE_WHITESPACE +REPORT_UDIFF
==== ======== ========================= =============
 ID   Member   Product                   valid until
---- -------- ------------------------- -------------
 1    Fred     Bread
 2    Fred     Buckwheat
 3    Anne     Buckwheat
 4    Henri    Electricity repair work
 5    Argo     Electricity repair work
==== ======== ========================= =============
<BLANKLINE>


The *ActiveProducts* table is an example of how to handle customized
complex filter conditions.  It is a subclass of `Products`, but adds
filter conditions so that only "active" products are shown, i.e. for
which there is at least one offer or one demand.  It also specifies
`column_names` to show the two virtual fields `offered_by` and
`wanted_by`.

>>> rt.show(market.ActiveProducts)
... #doctest: +NORMALIZE_WHITESPACE +REPORT_UDIFF
========================= ============================= =============================
 Designation               Offered by                    Wanted by
------------------------- ----------------------------- -----------------------------
 Bread                     `Fred <…>`__
 Buckwheat                 `Anne <…>`__, `Fred <…>`__    `Henri <…>`__
 Eggs                                                    `Henri <…>`__, `Mari <…>`__
 Electricity repair work   `Argo <…>`__, `Henri <…>`__
========================= ============================= =============================
<BLANKLINE>
