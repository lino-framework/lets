.. doctest docs/specs/db.rst
.. _lets.specs.db:

================================
Database structure of Lino LETS
================================

This document describes the database structure.

.. contents::
  :local:


.. include:: /../docs/shared/include/tested.rst

>>> import lino
>>> lino.startup('lino_lets.projects.letsdemo.settings.doctests')
>>> from lino.api.doctest import *


>>> analyzer.show_db_overview()
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
12 plugins: lino, about, jinja, bootstrap3, extjs, printing, system, users, market, lets, staticfiles, sessions.
8 models:
=================== ==================== ========= =======
 Name                Default table        #fields   #rows
------------------- -------------------- --------- -------
 market.Demand       market.Demands       3         3
 market.Offer        market.Offers        4         5
 market.Place        market.Places        2         4
 market.Product      market.Products      2         6
 sessions.Session    users.Sessions       3         0
 system.SiteConfig   system.SiteConfigs   3         0
 users.Authority     users.Authorities    3         0
 users.User          users.AllUsers       22        9
=================== ==================== ========= =======
<BLANKLINE>
