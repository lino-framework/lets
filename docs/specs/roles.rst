.. doctest docs/specs/roles.rst
.. _lets.specs.roles:

========================
User roles in Lino LETS
========================

>>> import lino
>>> lino.startup('lino_lets.projects.letsdemo.settings.doctests')
>>> from lino.api.doctest import *

Lino LETS uses the default user types defined in :mod:`lino.core.user_types`.

>>> settings.SITE.user_types_module
'lino.core.user_types'

>>> rt.show(users.UserTypes)
======= =========== ===============
 value   name        text
------- ----------- ---------------
 000     anonymous   Anonymous
 100     user        User
 900     admin       Administrator
======= =========== ===============
<BLANKLINE>

>>> rt.show(users.UserRoles)
================ ===== ===== =====
 Name             000   100   900
---------------- ----- ----- -----
 core.SiteAdmin               ☑
 core.SiteUser          ☑     ☑
================ ===== ===== =====
<BLANKLINE>


Menus
-----

Site manager
------------------

Rolf is a :term:`site manager`, he has a complete menu:

>>> ses = rt.login('robin')
>>> ses.user.user_type
<users.UserTypes.admin:900>

>>> show_menu('robin')
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
- Master : Members, Products
- Market : Offers, Demands
- Configure :
  - Places
  - System : Members, Site Parameters
- Explorer :
  - System : Authorities, User types, User roles
- Site : About, User sessions
