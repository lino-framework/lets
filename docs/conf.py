# -*- coding: utf-8 -*-

from atelier.sphinxconf import configure

configure(globals())
from lino.sphinxcontrib import configure

configure(globals(), 'lino_lets.projects.letsdemo.settings.doctests')

extensions += ['lino.sphinxcontrib.help_texts_extractor']
help_texts_builder_targets = {'lino_lets.': 'lino_lets.lib.lets'}

project = "Lino LETS"
copyright = '2016-2021 Rumma & Ko Ltd'

html_title = "Lino LETS"
