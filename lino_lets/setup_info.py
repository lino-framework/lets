# -*- coding: UTF-8 -*-
# Copyright 2016-2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

SETUP_INFO = dict(name='lino-lets',
                  version='20.9.0',
                  install_requires=['lino-xl'],
                  description=("A Local Exchange Trade System"),
                  author='Rumma & Ko Ltd',
                  author_email='info@lino-framework.org',
                  url="https://gitlab.com/lino-framework/lets",
                  license_files=['COPYING'],
                  test_suite='tests')

SETUP_INFO.update(long_description="""

A Lino application for people who exchange goods and services without money.

This repository is used by the
`Lino LETS tutorial
<https://www.lino-framework.org/dev/lets/index.html>`__.


""")

SETUP_INFO.update(classifiers="""
Programming Language :: Python
Programming Language :: Python :: 3
Development Status :: 4 - Beta
Environment :: Web Environment
Framework :: Django
Intended Audience :: Developers
Intended Audience :: System Administrators
Intended Audience :: Information Technology
Intended Audience :: Customer Service
License :: OSI Approved :: GNU Affero General Public License v3
Operating System :: OS Independent
Topic :: Software Development :: Bug Tracking
""".format(**SETUP_INFO).strip().splitlines())
SETUP_INFO.update(packages=[
    'lino_lets',
    'lino_lets.lib',
    'lino_lets.lib.lets',
    'lino_lets.lib.users',
    'lino_lets.lib.users.fixtures',
    'lino_lets.projects',
    'lino_lets.projects.letsdemo',
    'lino_lets.projects.letsdemo.tests',
    'lino_lets.projects.letsdemo.settings',
    'lino_lets.projects.letsdemo.settings.fixtures',
])

SETUP_INFO.update(package_data=dict())
