# -*- coding: UTF-8 -*-
# Copyright 2020 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""The standard project for Lino LETS.

.. autosummary::
   :toctree:

   settings

"""
