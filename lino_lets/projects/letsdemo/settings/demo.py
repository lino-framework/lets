# -*- coding: UTF-8 -*-
# Copyright 2016-2020 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""Defines and instantiates a demo version of a Lino LETS Site."""

# import datetime

from ..settings import *


class Site(Site):

    title = "My Lino LETS site"
    is_demo_site = True

# the_demo_date = datetime.date(2015, 5, 23)

# default_ui = 'lino_react.react'

SITE = Site(globals())

DEBUG = True

# the following line should not be active in a checked-in version
#~ DATABASES['default']['NAME'] = ':memory:'
