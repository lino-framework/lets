# -*- coding: UTF-8 -*-
# Copyright 2020 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""
Lino LETS extension of :mod:`lino.modlib.users`.

"""

from lino.modlib.users import Plugin


class Plugin(Plugin):
    "Extends the User model."
    extends_models = ['User']
