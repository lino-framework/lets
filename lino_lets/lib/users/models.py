# -*- coding: UTF-8 -*-
# Copyright 2020 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""Add a `place` field to the `users.User` model.

"""

from lino.api import dd, _

from lino.modlib.users.models import *


class User(User):

    class Meta(User.Meta):
        abstract = dd.is_abstract_model(__name__, 'User')
        verbose_name = _("Member")
        verbose_name_plural = _("Members")

    place = dd.ForeignKey('market.Place', blank=True, null=True)


from .ui import *
