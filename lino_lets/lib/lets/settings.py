from lino.projects.std.settings import *
from lino.ad import _


class Site(Site):

    verbose_name = "Lino LETS (step 1)"
    url = "https://www.lino-framework.org/dev/lets/"
    demo_fixtures = ['demo', 'demo2']

    # demo_fixtures = ['std', 'demo', 'demo2']
    # user_types_module = 'lino_lets.lib.lets.user_types'

    # workflows_module = 'lino_lets.lib.lets.workflows'
    # custom_layouts_module = 'lino_lets.lib.lets.layouts'
    # migration_class = 'lino_lets.lib.lets.migrate.Migrator'

    # default_ui = 'lino_react.react'

    def get_installed_plugins(self):
        yield super().get_installed_plugins()
        yield 'lino_lets.lib.users'
        yield 'lino_lets.lib.market'
        yield 'lino_lets.lib.lets'

    def setup_menu(self, profile, main, ar=None):

        m = main.add_menu("master", _("Master"))
        m.add_action('users.AllUsers')
        m.add_action('market.Products')

        m = main.add_menu("market", _("Market"))
        m.add_action('market.Offers')
        m.add_action('market.Demands')

        m = main.add_menu("config", _("Configure"))
        m.add_action('market.Places')
        super().setup_menu(profile, main, ar)

    def setup_quicklinks(self, user, tb):
        super().setup_quicklinks(user, tb)
        tb.add_action('market.Products')

    def get_dashboard_items(self, user):
        yield self.models.market.ActiveProducts
