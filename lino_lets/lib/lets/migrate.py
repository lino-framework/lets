# -*- coding: UTF-8 -*-
# Copyright 2020 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""Default data migrator for Lino LETS.


"""

from django.conf import settings
from lino.api import dd, rt
from lino.utils.dpy import Migrator, override


class Migrator(Migrator):
    """The standard migrator for Lino LETS.

    This is used because
    :class:`lino_lets.projects.letsdemo.settings.Site` has
    :attr:`migration_class <lino.core.site.Site.migration_class>` set
    to ``"lino_lets.lib.lets.migrate.Migrator"``.

    """

    def migrate_from_0_0_1(self, globals_dict):
        # do something here
        return '0.0.2'
