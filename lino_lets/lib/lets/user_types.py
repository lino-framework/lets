# -*- coding: UTF-8 -*-
# Copyright 2020 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""Defines the standard user roles for Lino LETS."""

from lino.core.roles import UserRole, SiteAdmin, SiteUser, SiteStaff
from lino.modlib.users.choicelists import UserTypes
from lino.api import _

UserTypes.clear()
add = UserTypes.add_item
add('000',
    _("Anonymous"),
    UserRole,
    'anonymous',
    readonly=True,
    authenticated=False)
add('100', _("User"), SiteUser, 'user')
add('500', _("Staff"), SiteStaff, 'staff')
add('900', _("Administrator"), SiteAdmin, 'admin')
