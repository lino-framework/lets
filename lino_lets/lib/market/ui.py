# -*- coding: UTF-8 -*-
# Copyright 2020 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from django.db.models import Q
from lino.api import dd


class Places(dd.Table):
    model = 'market.Place'


class Products(dd.Table):
    model = 'market.Product'
    order_by = ['name']

    detail_layout = """
    id name
    OffersByProduct DemandsByProduct
    """

    column_names = 'id name providers customers'


class ActiveProducts(Products):

    label = "Active products"
    column_names = 'name offered_by wanted_by'

    @classmethod
    def get_request_queryset(cls, ar):
        # add filter condition to the queryset so that only active
        # products are shown, i.e. for which there is at least one
        # offer or one demand.
        qs = super().get_request_queryset(ar)
        qs = qs.filter(Q(offer__isnull=False) | Q(demand__isnull=False))
        qs = qs.distinct()
        return qs


class Offers(dd.Table):
    model = 'market.Offer'
    column_names = "id provider product valid_until *"


class OffersByProvider(Offers):
    master_key = 'provider'
    column_names = "id product valid_until *"


class OffersByProduct(Offers):
    master_key = 'product'


class Demands(dd.Table):
    model = 'market.Demand'
    column_names = "id customer product *"


class DemandsByCustomer(Demands):
    master_key = 'customer'
    column_names = "product id *"


class DemandsByProduct(Demands):
    master_key = 'product'
    column_names = "customer id *"
