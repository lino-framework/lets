# -*- coding: UTF-8 -*-
# Copyright 2020-2023 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from django.db import models
from lino.api import dd, _
from lino.utils import join_elems
from lino.utils.html import E
from lino.core.actors import qs2summary
from lino.utils.mldbc.mixins import BabelNamed


class Place(BabelNamed):

    class Meta:
        verbose_name = _("Place")
        verbose_name_plural = _("Places")


class Product(BabelNamed):

    class Meta:
        verbose_name = _("Product")
        verbose_name_plural = _("Products")

    providers = models.ManyToManyField('users.User',
                                       verbose_name="Offered by",
                                       through='market.Offer',
                                       related_name='offered_products')
    customers = models.ManyToManyField('users.User',
                                       verbose_name="Wanted by",
                                       through='market.Demand',
                                       related_name='wanted_products')

    @dd.displayfield("Offered by")
    def offered_by(self, ar):
        if ar is None:
            return ''
        return qs2summary(ar, self.providers.all())

    @dd.displayfield("Wanted by")
    def wanted_by(self, ar):
        if ar is None:
            return ''
        return qs2summary(ar, self.customers.all())


class Offer(dd.Model):

    class Meta:
        verbose_name = _("Offer")
        verbose_name_plural = _("Offers")

    provider = dd.ForeignKey('users.User')
    product = dd.ForeignKey(Product)
    valid_until = models.DateField(blank=True, null=True)

    def __str__(self):
        return "%s offered by %s" % (self.product, self.provider)


class Demand(dd.Model):

    class Meta:
        verbose_name = _("Demand")
        verbose_name_plural = _("Demands")

    customer = dd.ForeignKey('users.User')
    product = dd.ForeignKey(Product)

    def __str__(self):
        return "%s (%s)" % (self.product, self.customer)


from .ui import *
