# -*- coding: UTF-8 -*-
# Copyright 2016-2020 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""This is the main module of Lino LETS.

.. autosummary::
   :toctree:

   lib
   projects


"""

from .setup_info import SETUP_INFO

__version__ = SETUP_INFO.get('version')

# intersphinx_urls = dict(docs="https://lets.lino-framework.org")
srcref_url = 'https://gitlab.com/lino-framework/lets/blob/master/%s'
doc_trees = ['docs']
